# WienerNetze SmartMeter InfluxDB Importer

Unofficial WienerNetze SmartMeter InfluxDB Importer based on https://github.com/platysma/vienna-smartmeter using the Wiener Netze Smart Meter private API.

## Installation

Install with pip and virtualenv:

```bash
python3 -m venv env
. env/bin/activate
pip install -r requirements.txt
```

## Usage

Set the parameters in `import_data.py`

```
# Logwien credentials
logwien_username = ''
logwien_password = ''

# collect power usage for one day x days ago
usage_day_delta = 3

# InfluxDB preferences
influxdb_host = ''
influxdb_port = 8086
influxdb_database = ''
influxdb_username = ''
influxdb_password = ''
influxdb_measurename = 'wienernetze.power.usage'
```

For regular data import create a cronjob. For example:
```
0 20 * * * /path/env/bin/python /path/import_data.py > /dev/null
```
