#!/bin/env python3

from datetime import date, timedelta, datetime, timezone
from vienna_smartmeter import Smartmeter
from influxdb import InfluxDBClient

# Logwien credentials
logwien_username = ''
logwien_password = ''

# collect power usage for one day x days ago
usage_day_delta = 3

# InfluxDB preferences
influxdb_host = ''
influxdb_port = 8086
influxdb_database = ''
influxdb_username = ''
influxdb_password = ''
influxdb_measurename = 'wienernetze.power.usage'

request_datetime = date.today() - timedelta(days = usage_day_delta)
start_date = datetime.strptime(f"{request_datetime} 00:00:00", '%Y-%m-%d %H:%M:%S')

api = Smartmeter(logwien_username, logwien_password)

data = api.verbrauch(start_date.astimezone(timezone.utc))

records = []
for value in data['values']:
    valuetime = datetime.strptime(value['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ').replace(tzinfo=timezone.utc).astimezone()

    record = {
        "measurement": influxdb_measurename,
        "time": value['timestamp'],
        "fields": {
            "value": value['value']
        },
    }

    records.append(record)

influx_client = InfluxDBClient(host=influxdb_host, port=influxdb_port, username=influxdb_username, password=influxdb_password)
influx_client.switch_database(influxdb_database)

influx_client.write_points(records)
